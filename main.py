from website import create_app
from flask_talisman import Talisman
from flask_bcrypt import Bcrypt
from flask_wtf.csrf import CSRFProtect

app = create_app()

#Talisman adds CSP header and HSTS
talisman = Talisman(app)

#Flask-WTF adds CSRF protection
csrf = CSRFProtect(app)

#Bcrypt adds password hashing
bcrypt = Bcrypt(app)

if __name__ == '__main__':
    app.run(host='0.0.0.0', ssl_context='adhoc')

# for self signed certs. run in terminal: openssl req -x509 -newkey rsa:4096 -nodes -out cert.pem -keyout key.pem -days 365
# app.run(ssl_context=('cert.pem', 'key.pem'))

