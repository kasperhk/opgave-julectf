import logging

from flask import Blueprint, render_template, request, flash, redirect, url_for
from .models import User
from . import db
from flask_login import login_user, login_required, logout_user, current_user
import re
from flask_bcrypt import Bcrypt

auth = Blueprint('auth', __name__)


@auth.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        user = User.query.filter_by(username=username).first()

        if user:
            if Bcrypt().check_password_hash(user.password, password.encode('utf-8')):
                login_user(user, remember=True)
                flash('logged in successfully!', category='success')
                # Password read once: Password = None
                password = None
                return redirect(url_for('views.profile'))
            else:
                flash('incorrect password, try again.', category='error')
                logging.error(f"Incorrect password from user: {username} IP: {request.remote_addr}")
        else:
            flash('username does not exist.', category='error')
            logging.error(f"Username does not exist: {username} IP: {request.remote_addr}")
    return render_template("login.html", user=current_user)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    flash('logged out successfully!', category='success')
    return redirect(url_for('views.index'))


@auth.route('/create', methods=['GET', 'POST'])
def create():
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        repassword = request.form.get('repassword')
        user = User.query.filter_by(username=username).first()

        if validate_user(username, password, repassword, user) == True:
            new_user = User(username=username, password=Bcrypt().generate_password_hash(password).decode('utf-8'))
            db.session.add(new_user)
            db.session.commit()
            # read once passwords
            password = None
            repassword = None
            flash('account created', category='success')
            return redirect(url_for('views.profile'))
        elif validate_user(username, password, repassword, user) != True:
            flash(validate_user(username, password, repassword, user), category='error')
    return render_template("create.html", user=current_user)


def validate_user(username, password, repassword, user):
    # regex: contain 1 Upper, 1 lower, 1 number, 1 special character, 8 characters
    pattern = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"

    if user:
        return "Username already exists."
    elif len(username) < 5:
        return "Username must be at least 5 characters."
    elif password != repassword:
        return "Passwords don't match."
    elif not re.match(pattern, password):
        return "Password must contain 1 Upper, 1 lower, 1 number, 1 special character, 8 characters"
    else:
        return True
